# uplb-hci-lab



# Installation

You should have the following:
- Node version 10 or 12

To install run,
```bash
npm install
```

# Usage

To run this project

```bash
npm start
```

# Documentation

Please visit the repo page or if you want to build your own copy here, you have to install Hugo globally

```bash
brew install hugo
```

Then run the following commands:

```bash
npm run serve-docs
```

If you want to just build the docs,

```bash
npm run build-docs
```
